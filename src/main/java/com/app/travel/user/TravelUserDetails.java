package com.app.travel.user;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import org.springframework.stereotype.Component;

import com.app.travel.dto.ProviderDTO;
import com.app.travel.dto.VisitedLocationDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import gpsUtil.location.VisitedLocation;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tripPricer.Provider;

@Component
@Data
@NoArgsConstructor
public class TravelUserDetails implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UUID userId;
	@JsonProperty(value = "usernamestring")
	private String userNameString;
	private String phoneNumberString;
	private String emailAddressString;
	@Getter(value = AccessLevel.NONE)
	@Setter(value = AccessLevel.NONE)
	private Timestamp latestLocationTimestamp;
	@Getter(value = AccessLevel.NONE)
	@Setter(value = AccessLevel.NONE)
	private List<VisitedLocationDTO> visitedLocations = new CopyOnWriteArrayList<>();

	private volatile List<UserReward> userRewards = new ArrayList<>();
	private UserPreferences userPreferences;
	private List<ProviderDTO> tripDeals = new CopyOnWriteArrayList<>();

	public TravelUserDetails(String string) {
		this.userNameString = string;
	}

	public TravelUserDetails(UUID randomUUID, String userName, String phone, String email) {
		this.userId = randomUUID;
		this.userNameString = userName;
		this.phoneNumberString = phone;
		this.emailAddressString = email;
	}

	public void addToVisitedLocations(VisitedLocation visitedLocation) {
		visitedLocations.add(new VisitedLocationDTO(visitedLocation));
	}

	public List<VisitedLocation> getVisitedLocations() {
		List<VisitedLocation> returnedList = new ArrayList<>();
		if (visitedLocations.size() > 0) {
			for (VisitedLocationDTO v : List.copyOf(visitedLocations)) {
				returnedList.add(v.getVisitedLocation());
			}
		}
		return returnedList;
	}

	public void setVisitedLocations(List<VisitedLocation> visitedLocations) {
		List<VisitedLocation> bufferList = List.copyOf(visitedLocations);
		List<VisitedLocationDTO> returnedList = new CopyOnWriteArrayList<>();
		bufferList.forEach(visitedLoc -> {
			VisitedLocationDTO visitedLocDTO = new VisitedLocationDTO(visitedLoc);;
			returnedList.add(visitedLocDTO);
			});
		this.visitedLocations = returnedList;
	}

	public void clearVisitedLocations() {
		visitedLocations.clear();
	}

	public void addUserReward(UserReward userReward) {
		if (java.util.Objects.nonNull(userReward)) {
			userRewards.add(userReward);
		}
	}

	public List<UserReward> getUserRewards() {
		List<UserReward> copy = new ArrayList<UserReward>();
		if (java.util.Objects.nonNull(userRewards)) {
			List<UserReward> buffer = List.copyOf(userRewards);
			buffer.forEach(ur -> copy.add(ur));
		}
		return copy;
	}

	public void setUserRewards(List<UserReward> rewardList) {
		List<UserReward> copy = new ArrayList<UserReward>();
		if (java.util.Objects.nonNull(userRewards)) {
			List<UserReward> buffer = List.copyOf(userRewards);
			
			buffer.forEach(ur -> copy.add(ur));
		}
		this.userRewards = copy;
	}

	public UserPreferences getUserPreferences() {
		if(this.userPreferences == null) {
			UserPreferences copy = new UserPreferences();
			if(this.userId != null) {
			copy.setPreferencesId(this.userId);
			}
			return copy;
		}else {
			return (UserPreferences) this.userPreferences.clone();
		}
	}

	public void setUserPreferences(UserPreferences userPreferences) {
		if(userPreferences != null) {
		this.userPreferences = (UserPreferences) userPreferences.clone();
		}
	}

	public VisitedLocation getLastVisitedLocation() {
		if (visitedLocations.size() > 0) {
			return visitedLocations.get(visitedLocations.size() - 1).getVisitedLocation();
		}
		return new VisitedLocation(userId, null, latestLocationTimestamp);
	}

	public void setTripDeals(List<Provider> tripDeals) {
		List<Provider> buffer = List.copyOf(tripDeals);
		List<ProviderDTO> copy = new ArrayList<>();
		buffer.forEach(b -> copy.add(new ProviderDTO(b)));
		this.tripDeals = copy;
	}

	public List<Provider> getTripDeals() {
		List<ProviderDTO> buffer = List.copyOf(this.tripDeals);
		List<Provider> copy = new ArrayList<>();
		buffer.forEach(b -> copy.add(b.getProvider()));
		return copy;
	}

	public void setLatestLocationTimestamp(Timestamp timestamp) {
		this.latestLocationTimestamp = (Timestamp) timestamp.clone();
	}

	public Timestamp getLatestLocationTimestamp() {
		if(this.latestLocationTimestamp != null) {
		return (Timestamp) this.latestLocationTimestamp.clone();
	}
		return null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(emailAddressString, phoneNumberString, userId, userNameString);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TravelUserDetails other = (TravelUserDetails) obj;
		return Objects.equals(emailAddressString, other.emailAddressString)
				&& Objects.equals(phoneNumberString, other.phoneNumberString) && Objects.equals(userId, other.userId)
				&& Objects.equals(userNameString, other.userNameString);
	}

	public Object clone() {
		TravelUserDetails copy = null;
		try {
			copy = (TravelUserDetails) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return copy;
	}

}
