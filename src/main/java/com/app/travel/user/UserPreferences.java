package com.app.travel.user;

import java.io.Serializable;
import java.util.UUID;

import javax.money.Monetary;
import javax.money.CurrencyUnit;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.javamoney.moneta.Money;

@Data
@NoArgsConstructor
public class UserPreferences implements Serializable, Cloneable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UUID preferencesId;
	private int attractionProximity = Integer.MAX_VALUE;
	@org.springframework.data.annotation.Transient
	private String currency = "USD";//Monetary.getCurrency("USD");
	private Money lowerPricePoint = Money.of(0.0, currency);
	private Money highPricePoint = Money.of(Double.MAX_VALUE, currency);
	private int tripDuration = 1;
	private int ticketQuantity = 1;
	private int numberOfAdults = 1;
	private int numberOfChildren = 0;
	
	public void setLowerPricePoint(Money money) {
		this.lowerPricePoint = Money.of(money.getNumber(), money.getCurrency());
	}
	
	public Money getLowerPricePoint() {
		return Money.of(this.lowerPricePoint.getNumber(), this.lowerPricePoint.getCurrency());
	}
	
	public void setHighPricePoint(Money money) {
		this.highPricePoint = Money.of(money.getNumber(), money.getCurrency());
	}
	
	public Money getHighPricePoint() {
		return Money.of(this.highPricePoint.getNumber(), this.highPricePoint.getCurrency());
	}

	public Object clone() {
		UserPreferences copy = null;
		try {
			copy = (UserPreferences) super.clone();
		}catch(CloneNotSupportedException c) {
			c.printStackTrace();
		}
		
		//copy.currency = Monetary.getCurrency(this.currency.getCurrencyCode());
		copy.lowerPricePoint = this.getLowerPricePoint();
		copy.highPricePoint = this.getHighPricePoint();
		
		return copy;
	}
	
}
