package com.app.travel.user;

import java.io.Serializable;
import java.util.Objects;

import com.app.travel.dto.AttractionDTO;
import com.app.travel.dto.VisitedLocationDTO;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Data
public class UserReward implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter(value = AccessLevel.NONE)
	private final VisitedLocationDTO visitedLocationDto;
	private final AttractionDTO attraction;
	private final int rewardPoints;
	
	public UserReward(VisitedLocation visitedLocation, Attraction attraction, int rewardPoints) {
		this.visitedLocationDto = new VisitedLocationDTO(visitedLocation);
		this.attraction = new AttractionDTO(attraction);
		this.rewardPoints = rewardPoints;

	}
	
/*	public UserReward(VisitedLocation visitedLocation, Attraction attraction) {
		this.visitedLocation = visitedLocation;
		this.attraction = attraction;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}*/
	
	public Attraction getAttraction() {
		return attraction.getAttraction();
	}
	
	public int getRewardPoints() {
		return rewardPoints;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserReward other = (UserReward) obj;
		return Objects.equals(attraction, other.attraction) && rewardPoints == other.rewardPoints
				&& Objects.equals(visitedLocationDto, other.visitedLocationDto);
	}

	@Override
	public int hashCode() {
		return Objects.hash(attraction, rewardPoints, visitedLocationDto);
	}
	
	
	
}
