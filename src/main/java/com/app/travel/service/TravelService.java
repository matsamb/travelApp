package com.app.travel.service;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;

import com.app.travel.dto.AttractionDetailsDTO;
import com.app.travel.helper.InternalTestHelper;
import com.app.travel.tracker.Tracker;
import com.app.travel.user.TravelUserDetails;
import com.app.travel.user.UserPreferences;
import com.app.travel.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TravelService {

	private final static Logger LOGGER = LogManager.getLogger("TravelService");

	public final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	public final SecureRandom secureRandom = new SecureRandom();
	boolean testMode = true;

	public TravelService(RewardsService rewardsService) {
		this.rewardsService = rewardsService;
		tracker = new Tracker(this, rewardsService);
		LOGGER.debug("Tracker started: " + tracker.getName() + ", state: " + tracker.getState());
		addShutDownHook();
	}

	public Boolean getTestMode() {
		return testMode;
	}

	public void setTestMode(Boolean bool) {
		testMode = bool;
	}

	public List<UserReward> getUserRewards(TravelUserDetails user) {
		LOGGER.info("getUserRewards: " + user.getUserRewards());
		return user.getUserRewards();
	}

	public VisitedLocation getUserLocation(TravelUserDetails user) {
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ? user.getLastVisitedLocation()
				: trackUserLocation(user);
		return visitedLocation;

	}

//	@SuppressWarnings("unlikely-arg-type")
	public List<Attraction> getNearByAttractions(VisitedLocation visitedLocation) {
		GpsUtil gpsUtil = new GpsUtil();

		Map<Attraction, Double> attractionDistanceMap = new HashMap<>();

		gpsUtil.getAttractions()
				.forEach(a -> attractionDistanceMap.put(a, rewardsService.getDistance(a, visitedLocation.location)));
		LOGGER.debug("attractionDistanceMap " + attractionDistanceMap);

		List<Double> closestAttractionDistanceList = attractionDistanceMap.values().stream().sorted()
				.collect(Collectors.toList()).subList(0, 5);
		LOGGER.debug("closestAttractionDistanceList " + closestAttractionDistanceList);
		List<Attraction> fiveAttractionsList = new ArrayList<>();

		for (Double d : closestAttractionDistanceList) {
			Map.Entry<Attraction, Double> mRemove = null;
			for (Map.Entry<Attraction, Double> m : attractionDistanceMap.entrySet()) {
				LOGGER.debug("loop attractionDistanceMap entry set");
				if (m.getValue().equals(d)) {
					LOGGER.debug("adding attraction " + m.getKey() + ", to attraction list");
					fiveAttractionsList.add(m.getKey());
					mRemove = m;
				}
			}
			attractionDistanceMap.remove(mRemove.getKey());
		}

		LOGGER.info("Attractions: " + fiveAttractionsList);
		return fiveAttractionsList;
	}

	public List<AttractionDetailsDTO> getNearAttractionsDetails(TravelUserDetails user) {

		List<AttractionDetailsDTO> fiveAttractionsDetailsList = new ArrayList<>();
		AttractionDetailsDTO attractionDetails = new AttractionDetailsDTO();
		Location userLocation = new Location(user.getLastVisitedLocation().location.latitude,
				user.getLastVisitedLocation().location.longitude);
		attractionDetails.setUserLatitude(userLocation.latitude);
		attractionDetails.setUserLongitude(userLocation.longitude);

		for (Attraction a : getNearByAttractions(user.getLastVisitedLocation())) {
			attractionDetails.setName(a.attractionName);
			attractionDetails.setAttractionLatitude(a.latitude);
			attractionDetails.setAttractionLongitude(a.longitude);
			Location attractionLocation = new Location(a.latitude, a.longitude);

			attractionDetails.setRewardPoints(rewardsService.getRewardPoints(a, user));
			attractionDetails.setDistance(rewardsService.getDistance(attractionLocation, userLocation));

			fiveAttractionsDetailsList.add((AttractionDetailsDTO) attractionDetails.clone());
		}
		LOGGER.info("AttractionsDetails: " + fiveAttractionsDetailsList);
		return fiveAttractionsDetailsList;
	}

	public TravelUserDetails getUser(String userName) {
		return internalUserMap.get(userName);
	}

	public Collection<TravelUserDetails> getAllUsers() {
		return internalUserMap.values();
	}

	public List<TravelUserDetails> getListAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}

	public void addUser(TravelUserDetails user) {
		if (!internalUserMap.containsKey(user.getUserNameString())) {
			internalUserMap.put(user.getUserNameString(), user);
		}
	}

	public List<Provider> getTripDeals(TravelUserDetails user) throws Exception {
		LOGGER.info("getTripDeals: ");
		List<Provider> providers = new ArrayList<Provider>();

		for (Attraction a : getNearByAttractions(user.getLastVisitedLocation())) {
			int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
			LOGGER.info("cumulatativeRewardPoints: " + cumulatativeRewardPoints);
			providers = tripPricer.getPrice(tripPricerApiKey, a.attractionId,
					user.getUserPreferences().getNumberOfAdults(), user.getUserPreferences().getNumberOfChildren(),
					user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
			LOGGER.info("providers: " + providers.get(0).name);
			user.setTripDeals(providers);
			break;
		}
		return providers;
	}

	public VisitedLocation trackUserLocation(TravelUserDetails user) {
		LOGGER.debug("trackUserLocation ");
		VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(),
				new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime());

		user.addToVisitedLocations(visitedLocation);
		LOGGER.debug("trackUserLocation: " + user.getVisitedLocations().get(0).location);
		return visitedLocation;
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				tracker.stopTracking();
			}
		});
	}

	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes
	// internal users are provided and stored in memory
	public final Map<String, TravelUserDetails> internalUserMap = new HashMap<>();

	public void initializeInternalUsers() {

		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000" + i;
			String email = userName + "@tourGuide.com";
			TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			UserPreferences preferences = new UserPreferences();
			preferences.setPreferencesId(user.getUserId());
			LOGGER.debug("setUserPreferences for user");
			user.setUserPreferences(preferences);

			internalUserMap.put(userName, user);
		});
		LOGGER.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}

	private void generateUserLocationHistory(TravelUserDetails user) {
		IntStream.range(0, 2).forEach(i -> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(),
					new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}

	private double generateRandomLongitude() {
		double leftLimit = -180;
		double rightLimit = 180;
		return leftLimit + secureRandom.nextDouble() * (rightLimit - leftLimit);
	}

	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
		double rightLimit = 85.05112878;
		return leftLimit + secureRandom.nextDouble() * (rightLimit - leftLimit);
	}

	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(secureRandom.nextInt(30));
		return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}

}
