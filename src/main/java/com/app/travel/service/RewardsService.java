package com.app.travel.service;

import java.io.Serializable;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import com.app.travel.dto.GpsUtilDTO;
import com.app.travel.dto.RewardCentralDTO;
import com.app.travel.user.TravelUserDetails;
import com.app.travel.user.UserReward;

@Service
public class RewardsService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Logger LOGGER = LogManager.getLogger("RewardsService");

	private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	// proximity in miles

	private int defaultProximityBuffer = 15;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = Math.max(proximityBuffer, 150);
	private final SecureRandom secureRandom = new SecureRandom();
	private final GpsUtilDTO gpsUtil;
	private final RewardCentralDTO rewardsCentral;

	public RewardsService(GpsUtilDTO gpsUtil, RewardCentralDTO rewardCentral) {
		this.gpsUtil = gpsUtil;
		this.rewardsCentral = rewardCentral;
	}

	public void setProximityBuffer(int proximityBuffer) {
		LOGGER.debug("setProximityBuffer");
		this.proximityBuffer = proximityBuffer;
	}

	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}

	public void calculateRewards(TravelUserDetails user) {
		List<VisitedLocation> userLocations = user.getVisitedLocations();
		List<Attraction> attractions = gpsUtil.getAttractions();
		LOGGER.debug("calculateRewards");
		int count = 0;
		for (VisitedLocation visitedLocation : userLocations) {
			for (Attraction attraction : attractions) {
				if (true) {
					if (nearAttraction(visitedLocation, attraction)) {
						user.addUserReward(
								new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));

						LOGGER.debug("get count " + user.getUserRewards().get(count));
						count++;
					}
				}
			}
		}
	}

	public void calculateRewards(List<TravelUserDetails> users) {
		List<Attraction> attractions = gpsUtil.getAttractions();
		LOGGER.debug("calculating rewards with list parameters");

		for (TravelUserDetails user : users) {
			LOGGER.debug(user);
			int count = 0;
			for (VisitedLocation visitedLocation : user.getVisitedLocations()) {

				for (Attraction attraction : attractions) {
					LOGGER.debug(user.getVisitedLocations().get(0).location.longitude + " VS " + attraction.latitude);
					if (user.getUserRewards().stream()
							.filter(r -> r.getAttraction().attractionName.equals(attraction.attractionName))
							.count() == 0) {
						if (nearAttraction(visitedLocation, attraction)) {
							user.addUserReward(
									new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));

							LOGGER.debug("get count " + user.getUserRewards().get(count));
							count++;
						}
					}
				}
			}
		}
	}

	public void calculateLatestRewards(List<TravelUserDetails> users) {
		LOGGER.debug("calculating rewards with list parameters");

		for (TravelUserDetails user : users) {
			LOGGER.debug(user);
			int count = 0;
			VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(),
					new Location(secureRandom.nextDouble(-85.05, 85.05), secureRandom.nextDouble(-180.0, 180.0)),
					Date.from(LocalDateTime.now().minusDays(secureRandom.nextInt(30)).toInstant(ZoneOffset.UTC)));

			for (Attraction attraction : gpsUtil.getAttractions()) {
				LOGGER.debug(user.getVisitedLocations().get(0).location.longitude + " VS " + attraction.latitude);
					if (nearAttraction(visitedLocation, attraction)) {
						user.addUserReward(
								new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));

						LOGGER.debug("get count " + user.getUserRewards().get(count));
						count++;
					}
			}
 
		}
	}

	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		LOGGER.info("isWithinAttractionProximity");
		LOGGER.debug("Range " + attractionProximityRange);
		return getDistance(attraction, location) > attractionProximityRange ? false : true;
	}

	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		LOGGER.debug("nearAttraction");
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}

	public int getRewardPoints(Attraction attraction, TravelUserDetails user) {

		LOGGER.debug(rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId()));
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}

	public double getDistance(Location loc1, Location loc2) {

		LOGGER.debug("getDistance");

		double lat1 = Math.toRadians(loc1.latitude);

		double lon1 = Math.toRadians(loc1.longitude);

		double lat2 = Math.toRadians(loc2.latitude);

		double lon2 = Math.toRadians(loc2.longitude);
		LOGGER.trace("lat1" + lat1 + " lat2" + lat2 + " long1" + lon1 + " long2" + lon2);

		double angle = Math
				.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

		LOGGER.trace("angle " + angle);
		double nauticalMiles = 60 * Math.toDegrees(angle);
		LOGGER.trace("nauticalMiles " + nauticalMiles);
		double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
		LOGGER.trace("statutesMiles " + statuteMiles);
		return statuteMiles;

	}

}
