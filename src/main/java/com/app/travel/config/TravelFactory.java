package com.app.travel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.app.travel.dto.GpsUtilDTO;
import com.app.travel.dto.RewardCentralDTO;
import com.app.travel.service.RewardsService;

@Configuration
public class TravelFactory {
	
	@Bean
	public GpsUtilDTO getGpsUtil(){
		return new GpsUtilDTO();
	}
	
	@Bean
	public RewardsService getRewardsService() {
		return new RewardsService(getGpsUtil(), getRewardCentral());
	}
	
	@Bean
	public RewardCentralDTO getRewardCentral() {
		return new RewardCentralDTO();
	}
	
}
