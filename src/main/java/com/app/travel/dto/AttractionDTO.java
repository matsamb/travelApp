package com.app.travel.dto;

import java.io.Serializable;

import gpsUtil.location.Attraction;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AttractionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String attractionName;
	private String city;
	private String state;
	private double latitude; 
	private double longitude;
	
	public AttractionDTO(Attraction attraction) {
		this.attractionName = attraction.attractionName;
		this.city = attraction.city;
		this.latitude = attraction.latitude;
		this.longitude = attraction.longitude;
		this.state = attraction.state;
	}
	
	public Attraction getAttraction() {
	return	new Attraction(attractionName, city, state, latitude, longitude);
	}
	
}
