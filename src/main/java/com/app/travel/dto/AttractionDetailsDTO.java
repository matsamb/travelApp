package com.app.travel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AttractionDetailsDTO implements Cloneable{

	private String name;
	private double attractionLongitude;
	private double attractionLatitude;
	private double userLongitude;
	private double userLatitude;
	private double distance;
	private double rewardPoints;
	
	public Object clone() {
		AttractionDetailsDTO copy = null;
		try {
			copy = (AttractionDetailsDTO)super.clone();
		}catch(CloneNotSupportedException c) {
			c.printStackTrace();
		}
		return copy;
	}
	
	
}
