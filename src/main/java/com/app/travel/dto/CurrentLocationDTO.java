package com.app.travel.dto;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrentLocationDTO implements Cloneable{

	private UUID userId;
	private Double longitude;
	private Double latitude;
	
	public Object clone() {
		CurrentLocationDTO copy = null;
		
		try {
			copy = (CurrentLocationDTO)super.clone();
		}catch(CloneNotSupportedException c) {
			c.printStackTrace();
		}
		return copy;
	}
	
}
