package com.app.travel.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VisitedLocationDTO implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UUID userId;
	private Double longitude;
	private Double latitude;
	private Date timeVisited;

	public VisitedLocationDTO(UUID userId, Location location, Date timeVisited) {
		this.userId = userId;
		this.longitude = location.longitude;
		this.latitude = location.latitude;
		this.timeVisited = (Date) timeVisited.clone();
	}

	public VisitedLocationDTO(VisitedLocation visitedLocation) {
		this.userId = visitedLocation.userId;
		this.longitude = visitedLocation.location.longitude;
		this.latitude = visitedLocation.location.latitude;
		this.timeVisited = (Date) visitedLocation.timeVisited.clone();
	}
	
	public VisitedLocation getVisitedLocation() {
		Location loc = new Location(this.latitude, this.longitude);
		return new VisitedLocation(this.userId, loc, (Date) this.timeVisited.clone());
	}
	
	public void setTimeVisited(Date timeVisited) {
		this.timeVisited = (Date) timeVisited.clone();
	}
	
	public Date getTimeVisited() {
		return (Date) this.timeVisited.clone();
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitedLocationDTO other = (VisitedLocationDTO) obj;
		return Objects.equals(latitude, other.latitude) && Objects.equals(longitude, other.longitude)
				&& Objects.equals(timeVisited, other.timeVisited) && Objects.equals(userId, other.userId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(latitude, longitude, timeVisited, userId);
	}
	
	public Object clone() {
		VisitedLocationDTO copy = null;
		try {
			copy = (VisitedLocationDTO) super.clone();
		}catch(CloneNotSupportedException c) {
			c.printStackTrace();
		}
		copy.timeVisited = (Date)this.timeVisited.clone();
		return copy;		
	}


}
