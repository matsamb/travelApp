package com.app.travel.dto;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;
import lombok.NoArgsConstructor;
import tripPricer.Provider;

@Data
@NoArgsConstructor
public class ProviderDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UUID tripId;
	private String name;
	private double price;
	
	public ProviderDTO(Provider provider) {
		this.tripId = provider.tripId; 
		this.name = provider.name;
		this.price= provider.price;
	}

	public Provider getProvider() {
		return new Provider(this.tripId, this.name, this.price);
	}
}
