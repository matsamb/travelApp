package com.app.travel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.app.travel.service.RewardsService;
import com.app.travel.service.TravelService;
import com.app.travel.task.RewardsRecursiveAction;

import lombok.extern.log4j.Log4j2;

@SpringBootApplication
@Log4j2
public class TravelApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(TravelApplication.class, args);
	}

	@Autowired
	private TravelService travelService;

	@Autowired
	private RewardsService rewardsService;

	@Override
	public void run(String... args) throws Exception {
		if (travelService.getTestMode()) {
			log.debug("TestMode enabled");
			travelService.initializeInternalUsers();
			log.info("initializeInternalUsers");
			RewardsRecursiveAction rewardsRecursiveAction = new RewardsRecursiveAction(rewardsService,
					travelService.getListAllUsers());
			rewardsRecursiveAction.invoke();
			log.info("calculate rewards");
		}
	}

}
