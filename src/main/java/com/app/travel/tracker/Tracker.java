
package com.app.travel.tracker;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.app.travel.user.TravelUserDetails;
import com.app.travel.service.RewardsService;
import com.app.travel.service.TravelService;
import com.app.travel.task.LatestRewardsRecursiveAction;

@Component
public class Tracker extends Thread {
	private static final Logger LOGGER = LogManager.getLogger("Tracker");
	
	private static final long trackingPollingInterval = TimeUnit.MINUTES.toSeconds(5);
	private final ExecutorService executorService = Executors.newSingleThreadExecutor();
	public final TravelService travelService;
	public final RewardsService rewardsService;
	private boolean stop = false;

	public Tracker(TravelService travelService, RewardsService rewardsService) {
		this.travelService = travelService;
		this.rewardsService = rewardsService;
		executorService.submit(this);
		LOGGER.info("Tracker state : "+this.getState());
	}

	/**
	 * Assures to shut down the Tracker thread
	 */
	public void stopTracking() {
		LOGGER.info("stopTracking");
		stop = true;
		executorService.shutdownNow();
	}
	
	@Override
	public void run() {
		StopWatch stopWatch = new StopWatch();
		while(true) {
			if(Thread.currentThread().isInterrupted() || stop) {
				LOGGER.debug("Tracker stopping");
				break;
			}
			

			List<TravelUserDetails> users = travelService.getListAllUsers();
			LOGGER.debug("Begin Tracker. Tracking " + users.size() + " users.");

			stopWatch.start();
			LatestRewardsRecursiveAction latestRewardsRecursiveAction = new LatestRewardsRecursiveAction(rewardsService,
					travelService.getListAllUsers());
			latestRewardsRecursiveAction.invoke();
			stopWatch.stop();
			LOGGER.info("Tracker Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
			stopWatch.reset();
			try {
				LOGGER.debug("Tracker sleeping");
				TimeUnit.SECONDS.sleep(trackingPollingInterval);
			} catch (InterruptedException e) {
				break;
			}
		}
		
	} 
	
}
