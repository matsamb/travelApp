package com.app.travel.task;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.RecursiveAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.app.travel.service.RewardsService;
import com.app.travel.user.TravelUserDetails;

@Component
public class RewardsRecursiveAction extends RecursiveAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger LOGGER = LogManager.getLogger("RewardsRecursiveTask");
	private final Integer MAX_LIST_SIZE = 16;
	public List<TravelUserDetails> users;
	public RewardsService rewardsService;

	public RewardsRecursiveAction(RewardsService rewardsService, List<TravelUserDetails> users) {
		this.users = users;
		this.rewardsService = rewardsService;
	}

	@Override
	protected void compute() {

		LOGGER.debug("number of users: "+users.size());
		
		if (users.size() < MAX_LIST_SIZE) {
			LOGGER.debug("list size < "+ MAX_LIST_SIZE);
			computeOk(users);
		} else {
			int halfSize = users.size() / 2;
			
			LOGGER.debug("list size > " + MAX_LIST_SIZE);
			RewardsRecursiveAction task1 = new RewardsRecursiveAction(rewardsService, users.subList(0, halfSize - 1));
			RewardsRecursiveAction task2 = new RewardsRecursiveAction(rewardsService, users.subList(halfSize,users.size() - 1));
			
			invokeAll(task1,task2);
		}
	}
	
	protected void computeOk(List<TravelUserDetails> userDetails) {
		LOGGER.debug("computeOk");
		rewardsService.calculateRewards(userDetails);
	}

}
