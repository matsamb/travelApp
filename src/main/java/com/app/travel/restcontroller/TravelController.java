package com.app.travel.restcontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.travel.dto.AttractionDetailsDTO;
import com.app.travel.dto.CurrentLocationDTO;
import com.app.travel.service.RewardsService;
import com.app.travel.service.TravelService;
import com.app.travel.user.TravelUserDetails;
import com.app.travel.user.UserReward;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import tripPricer.Provider;

@RestController
public class TravelController {

	// TODO: Get a list of every user's most recent location as JSON
	// - Note: does not use gpsUtil to query for their current location,
	// but rather gathers the user's current location from their stored location
	// history.

	private static final Logger LOGGER = LogManager.getLogger("TravelController");

	@Autowired
	private TravelService travelService;  
	
	@Autowired
	private RewardsService rewardsService;

	TravelController(TravelService travelService
			, RewardsService rewardsService
			){
		this.rewardsService = rewardsService;
		this.travelService = travelService;
	}
	
/*	@GetMapping("/travel")
	public String getHome() {
		LOGGER.info("Home page displayed");
		return "Welcome to TravelApp";
	}*/

/*	@GetMapping("/users")
	public ResponseEntity<List<TravelUserDetails>> getUsers() {
		LOGGER.info("users page displayed");
		List<TravelUserDetails> users = travelService.getListAllUsers();
		LOGGER.info(users);
		return ResponseEntity.ok(users);
	}*/

	@GetMapping("/getAllAttractions")
	public ResponseEntity<List<AttractionDetailsDTO>> getAllAttractions(@RequestParam Optional<String> userName) {

		LOGGER.info("getNearbyAttractions");

		if (userName.isEmpty()) {
			LOGGER.info("Missing parameter unsernameOptional");
			return ResponseEntity.badRequest().build();
		} else {
			String username = userName.get();
			if (travelService.getUser(username).getUserNameString() == "Not_Registered") {
				LOGGER.info("User not found");
				return ResponseEntity.badRequest().build();
			} else {
				LOGGER.info("Found user: " + username);
				return ResponseEntity.ok(
						travelService.getNearAttractionsDetails(travelService.getUser(username)));
			}
		}
	}
    
    @GetMapping("/rewards")
	public ResponseEntity<List<UserReward>> getAllRewardsPara() {

		LOGGER.info("rewards page displayed");
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		List<UserReward> rewardList = new ArrayList<UserReward>();

		for (TravelUserDetails u : travelService.getAllUsers()) {
			LOGGER.debug("adding reward for " + u.getUserNameString());
			rewardList.addAll(u.getUserRewards());
		}
		stopWatch.stop();
		LOGGER.debug("search duration: "
				+ TimeUnit.MILLISECONDS.toMinutes(stopWatch.getTime()));

		return ResponseEntity.ok(rewardList);
	}

	@GetMapping("/visitedlocation")
	public ResponseEntity<VisitedLocation> getUsers(@RequestParam Optional<String> usernameOptional) {
		if (usernameOptional.isEmpty()) {
			LOGGER.info("Missing parameter usernameOptional");
			return ResponseEntity.badRequest().build();
		} else {

			String username = usernameOptional.get();
			if (travelService.getUser(username).getUserNameString() == "Not_Registered") {
				LOGGER.info("Not found");
				return ResponseEntity.notFound().build();
			} else {
				LOGGER.info("visitedlocation");
				TravelUserDetails user = travelService.getUser(username);
				LOGGER.debug("Found user with username: "+username);
				return ResponseEntity.ok(user.getLastVisitedLocation());
			}
		}
	}

	@GetMapping("/getTripDeals")
	public ResponseEntity<List<Provider>> getTripDeals(@RequestParam Optional<String> userName) throws Exception {
		LOGGER.info("getTripDeals");

		if (userName.isEmpty()) {
			LOGGER.info("Missing parameter usernameOptional");
			return ResponseEntity.badRequest().build();
		} else {
			String username = userName.get();
			if (travelService.getUser(username).getUserNameString() == "Not_Registered") {
				LOGGER.info("User not found");
				return ResponseEntity.notFound().build();
			} else {
				LOGGER.info("User not found");
				TravelUserDetails user = travelService.getUser(username);
				return ResponseEntity.ok(travelService.getTripDeals(user));
			}
		}
	}
	
	@GetMapping("/getRewards")
	public ResponseEntity<List<UserReward>> getRewards(@RequestParam Optional<String> usernameOptional) {
		LOGGER.info("getRewards");
		if (usernameOptional.isEmpty()) {
			LOGGER.info("Missing parameter usernameOptional");
			return ResponseEntity.badRequest().build();
		} else {
			String username = usernameOptional.get();
			if (travelService.getUser(username).getUserNameString() == "Not_Registered") {
				LOGGER.info("User not found");
				return ResponseEntity.notFound().build();
			} else {
				LOGGER.info("found user");
				TravelUserDetails user = travelService.getUser(username);
				rewardsService.calculateRewards(user);
				return ResponseEntity.ok(user.getUserRewards());
			}
		}
	}
	
	@GetMapping("/getNearbyAttractions")
	public ResponseEntity<List<Attraction>> getNearbyAttractions(@RequestParam Optional<String> userName) {

		LOGGER.info("getNearbyAttractions");

		if (userName.isEmpty()) {
			LOGGER.info("Missing parameter unsernameOptional");
			return ResponseEntity.badRequest().build();
		} else {
			String username = userName.get();
			if (travelService.getUser(username).getUserNameString() == "Not_Registered") {
				LOGGER.info("User not found");
				return ResponseEntity.badRequest().build();
			} else {
				LOGGER.info("Found user: " + username);

				return ResponseEntity.ok(
						travelService.getNearByAttractions(travelService.getUser(username).getLastVisitedLocation()));
			}
		}
	}
	
	
	@GetMapping("/getAllCurrentLocations")
	public ResponseEntity<List<CurrentLocationDTO>> getAllCurrentLocations(
			@RequestParam Optional<String> usernameOptional) {
		LOGGER.info("getAllCurrentLocations");
		if (usernameOptional.isEmpty()) {
			LOGGER.info("Missing parameter usernameOptional");
			return ResponseEntity.badRequest().build();
		} else {
			String username = usernameOptional.get();
			if (travelService.getUser(username).getUserNameString() == "Not_Registered") {
				LOGGER.info("User not found");
				return ResponseEntity.notFound().build();
			} else {

				LOGGER.info("getAllCurrentLocations for: " + username);
				TravelUserDetails user = travelService.getUser(username);
				CurrentLocationDTO currentDto = new CurrentLocationDTO();
				List<CurrentLocationDTO> currentDtoList = new ArrayList<>();
				currentDto.setUserId(user.getUserId());
				user.getVisitedLocations().forEach(l -> {
					currentDto.setLatitude(l.location.latitude);
					currentDto.setLongitude(l.location.longitude);
					currentDtoList.add((CurrentLocationDTO) currentDto.clone());
				});

				LOGGER.info("Current Locations for: " + currentDtoList);
				return ResponseEntity.ok(currentDtoList);
			}
		}
	}

	@GetMapping("/allvisitedlocation")
	public ResponseEntity<List<CurrentLocationDTO>> getAllUsersLocation() {
		LOGGER.info("allvisitedlocation");
		
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		
		List<CurrentLocationDTO> currentDtoList = new ArrayList<>();
		CurrentLocationDTO currentDto = new CurrentLocationDTO();
		for (TravelUserDetails u : travelService.getAllUsers()) {
			currentDto.setUserId(u.getUserId());
			u.getVisitedLocations().forEach(l -> {
				currentDto.setLatitude(l.location.latitude); 
				currentDto.setLongitude(l.location.longitude);
				currentDtoList.add((CurrentLocationDTO) currentDto.clone());
			});
		}
		
		stopWatch.stop();
		LOGGER.info("search duration: "
				+ TimeUnit.MILLISECONDS.toMinutes(stopWatch.getTime()));

		return ResponseEntity.ok(currentDtoList);
	}

}