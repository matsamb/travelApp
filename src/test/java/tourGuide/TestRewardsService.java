
package tourGuide;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


import org.junit.jupiter.api.Test;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import com.app.travel.dto.GpsUtilDTO;
import com.app.travel.dto.RewardCentralDTO;
import com.app.travel.service.RewardsService;
import com.app.travel.user.TravelUserDetails;
import com.app.travel.user.UserReward;

public class TestRewardsService {
		
	public GpsUtilDTO getGpsUtil(){
		return new GpsUtilDTO();
	}
	
	public RewardsService getRewardsService() {
		return new RewardsService(getGpsUtil(), getRewardCentral());
	}
	
	public RewardCentralDTO getRewardCentral() {
		return new RewardCentralDTO();
	}
	
	@Test
	public void givenAUserAtAnAttractionWithProximityLimit_whenCalculateRewards_thenUserRewardsListSizeShouldBeOne() {
	
		GpsUtil gpsUtil = getGpsUtil();
		
		RewardsService rewardsService = getRewardsService();
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Attraction attraction = gpsUtil.getAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date(System.currentTimeMillis())));

		rewardsService.calculateRewards(user);
		
		List<UserReward> userRewards = user.getUserRewards();
		
		assertThat(userRewards.size()).isEqualTo(1);
	}
	
	@Test
	public void givenTwoUserAtAnAttractionWithProximityLimit_whenCalculateRewards_thenEachUserRewardsListSizeShouldBeOne() {
	
		GpsUtil gpsUtil = getGpsUtil();
		
		RewardsService rewardsService = getRewardsService();
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		TravelUserDetails user2 = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		List<TravelUserDetails> listOfUsers = new ArrayList<TravelUserDetails>();
		
		Attraction attraction = gpsUtil.getAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date(System.currentTimeMillis())));
		user2.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date(System.currentTimeMillis())));
		listOfUsers.add(user);
		listOfUsers.add(user2);
		
		rewardsService.calculateRewards(listOfUsers);
		
		List<UserReward> userRewards = user.getUserRewards();	
	
		assertThat(userRewards.size()).isEqualTo(1);
	}
	

	
	@Test
	public void givenAUserAtAnAttractionWithProximityLimit_whenIsWithinAttractionProximity_thenItShouldBeTrue() {
		
		GpsUtil gpsUtil = getGpsUtil();
		RewardsService rewardsService = getRewardsService();
		Attraction attraction = gpsUtil.getAttractions().get(0);
		
		assertThat(rewardsService.isWithinAttractionProximity(attraction, attraction)).isTrue();
	}
	
	@Test
	public void givenAUserWithoutProximityLimit_whenCalculateRewards_thenNumberOfAttractionAndRewardsListSizeShouldBeTheSame() {
		
		GpsUtil gpsUtil = getGpsUtil();
		RewardsService rewardsService = getRewardsService();
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);
		

		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Attraction attraction = gpsUtil.getAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date(System.currentTimeMillis())));
	
		rewardsService.calculateRewards(user);
		List<UserReward> userRewards = user.getUserRewards();

		assertThat(gpsUtil.getAttractions().size()).isEqualTo(userRewards.size());
	}
	
}
