package tourGuide;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import com.app.travel.dto.AttractionDetailsDTO;
import com.app.travel.dto.GpsUtilDTO;
import com.app.travel.dto.RewardCentralDTO;
import com.app.travel.helper.InternalTestHelper;
import com.app.travel.service.RewardsService;
import com.app.travel.service.TravelService;
import com.app.travel.user.TravelUserDetails;
import com.app.travel.user.UserPreferences;

import tripPricer.Provider;

public class TravelServiceTest {

	public GpsUtilDTO getGpsUtil(){
		return new GpsUtilDTO();
	}
	
	public RewardsService getRewardsService() {
		return new RewardsService(getGpsUtil(), getRewardCentral());
	}
	
	public RewardCentralDTO getRewardCentral() {
		return new RewardCentralDTO();
	}

	@Test
	public void getUserLocation() {
	
		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(0);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		tourGuideService.tracker.stopTracking();
		
		assertThat(visitedLocation.userId.equals(user.getUserId())).isTrue();
	}
	
	@Test
	public void addUser() {

		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(0);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		TravelUserDetails user2 = new TravelUserDetails(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		TravelUserDetails retrivedUser = tourGuideService.getUser(user.getUserNameString());

		tourGuideService.tracker.stopTracking();
		
		assertThat(retrivedUser).isEqualTo(user);
	}
	
	@Test
	public void getAllUsers() {

		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(0);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		TravelUserDetails user2 = new TravelUserDetails(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		List<TravelUserDetails> allUsers = tourGuideService.getListAllUsers();

		tourGuideService.tracker.stopTracking();
		
		assertThat(allUsers.contains(user2)).isTrue();
	}
	
	@Test
	public void trackUser() {
	
		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(0);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		
		tourGuideService.tracker.stopTracking();
		
		assertThat(visitedLocation.userId).isEqualTo(user.getUserId());
	}
	
	@Test
	public void getNearbyAttractionsTest() {
		
		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(0);
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user);
		
		List<Attraction> attractions = tourGuideService.getNearByAttractions(visitedLocation);
		System.out.println(attractions);
		tourGuideService.tracker.stopTracking();
		
		assertThat(attractions.size()).isEqualTo(5);
	}
	
	@Test
	public void getTripDealsTest() throws Exception {

		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(1);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		
		List<VisitedLocation> l = new ArrayList<>();
		l.add(new VisitedLocation(UUID.randomUUID()
				, new Location(8.365, 65.02)
				, new Timestamp(System.currentTimeMillis())));

		user.setVisitedLocations(l);
		
		UserPreferences preferences = new UserPreferences();
		preferences.setPreferencesId(user.getUserId());
		user.setUserPreferences(preferences);		
		
		List<Provider> providers = tourGuideService.getTripDeals(user);
		tourGuideService.tracker.stopTracking();
		
		assertThat(providers.size()).isEqualTo(5);
	}
	
	@Test
	public void getNearAttractionsDetailsTest() {
		
		RewardsService rewardsService = getRewardsService();
		InternalTestHelper.setInternalUserNumber(0);
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);
		TravelService tourGuideService = new TravelService(rewardsService);
		
		TravelUserDetails user = new TravelUserDetails(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		List<VisitedLocation> l = new ArrayList<>();
		l.add(new VisitedLocation(UUID.randomUUID()
				, new Location(8.365, 65.02)
				, new Timestamp(System.currentTimeMillis())));

		user.setVisitedLocations(l);
		
		List<AttractionDetailsDTO> attractions = tourGuideService.getNearAttractionsDetails(user);
		
		assertThat(attractions.size()).isEqualTo(5);
	}
	
	
}
