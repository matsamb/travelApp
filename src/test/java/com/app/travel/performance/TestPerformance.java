
package com.app.travel.performance;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import com.app.travel.dto.CurrentLocationDTO;
import com.app.travel.dto.GpsUtilDTO;
import com.app.travel.dto.RewardCentralDTO;
import com.app.travel.service.RewardsService;
import com.app.travel.task.LatestRewardsRecursiveAction;
import com.app.travel.task.RewardsRecursiveAction;
import com.app.travel.user.TravelUserDetails;
import com.app.travel.user.UserPreferences;

public class TestPerformance {

	public GpsUtilDTO getGpsUtil(){
		return new GpsUtilDTO();
	}
	
	public RewardsService getRewardsService() {
		return new RewardsService(getGpsUtil(), getRewardCentral());
	}
	
	public RewardCentralDTO getRewardCentral() {
		return new RewardCentralDTO();
	}
	/*
	 * A note on performance improvements:
	 * 
	 * The number of users generated for the high volume tests can be easily
	 * adjusted via this method:
	 * 
	 * InternalTestHelper.setInternalUserNumber(100000);
	 * 
	 * 
	 * These tests can be modified to suit new solutions, just as long as the
	 * performance metrics at the end of the tests remains consistent.
	 * 
	 * These are performance metrics that we are trying to hit:
	 * 
	 * highVolumeTrackLocation: 100,000 users within 15 minutes:
	 * assertTrue(TimeUnit.MINUTES.toSeconds(15) >=
	 * TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 *
	 * highVolumeGetRewards: 100,000 users within 20 minutes:
	 * assertTrue(TimeUnit.MINUTES.toSeconds(20) >=
	 * TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */

	public TravelUserDetails getTravelUserDetails(UUID uuid, List<VisitedLocation> visitedLocation,
			Timestamp timestamp) {
		TravelUserDetails t = new TravelUserDetails();
		t.setUserId(uuid);
		t.setVisitedLocations(visitedLocation);
		t.setLatestLocationTimestamp(timestamp);
		return t;
	}

	public List<VisitedLocation> getRandomVisitedLocation() {
		List<VisitedLocation> l = new ArrayList<>();
		l.add(new VisitedLocation(UUID.randomUUID(), new Location(-58.256, 34.25),
				new Timestamp(System.currentTimeMillis())));
		return l;
	}

	public Timestamp getTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	@Test
	public void givenAllUsersAtRandomLocations_whenGetRewardsWith100000Users_thenItShouldReturnResultInLessThan20Minutes() {

		// Users should be incremented up to 100,000, and test finishes within 20
		// minutes
		// InternalTestHelper.setInternalUserNumber(10);

		List<TravelUserDetails> userList = new ArrayList<TravelUserDetails>();
		IntStream.range(0, 100000).forEach(e -> {
			TravelUserDetails t = getTravelUserDetails(UUID.randomUUID(), getRandomVisitedLocation(), getTimestamp());
			userList.add(t);
		});

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		RewardsService rewardService = getRewardsService();
		RewardsRecursiveAction rewardsRecursiveAction = new RewardsRecursiveAction(rewardService, userList);
		rewardsRecursiveAction.invoke();

		stopWatch.stop();
		assertThat(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())).isTrue();

	}

	@Test
	public void givenAllUsersAtRandomLocations_whenGetLatestRewardsWith100000Users_thenItShouldReturnResultInLessThan20Minutes() {

		// Users should be incremented up to 100,000, and test finishes within 20
		// minutes
		// InternalTestHelper.setInternalUserNumber(10);

		List<TravelUserDetails> userList = new ArrayList<TravelUserDetails>();
		IntStream.range(0, 100000).forEach(e -> {
			TravelUserDetails t = getTravelUserDetails(UUID.randomUUID(), getRandomVisitedLocation(), getTimestamp());
			userList.add(t);
		});

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		RewardsService rewardService = getRewardsService();
		LatestRewardsRecursiveAction rewardsRecursiveAction = new LatestRewardsRecursiveAction(rewardService, userList);
		rewardsRecursiveAction.invoke();

		stopWatch.stop();

		assertThat(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())).isTrue();

	}

	@Test
	public void givenAllUsers_whenGetAllVisitedLocationFor100000Users_thenItShouldReturnResultInLessThan15Minutes() {

		// Users should be incremented up to 100,000, and test finishes within 15
		// minutes

		List<TravelUserDetails> userList = new ArrayList<TravelUserDetails>();
		IntStream.range(0, 100000).forEach(e -> {
			TravelUserDetails user = getTravelUserDetails(UUID.randomUUID(), getRandomVisitedLocation(),
					getTimestamp());
			UserPreferences preferences = new UserPreferences();
			preferences.setPreferencesId(user.getUserId());
			user.setUserPreferences(preferences);
			userList.add(user);
		});

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		List<CurrentLocationDTO> currentDtoList = new ArrayList<>();
		CurrentLocationDTO currentDto = new CurrentLocationDTO();
		for (TravelUserDetails u : userList) {
			currentDto.setUserId(u.getUserId());
			u.getVisitedLocations().forEach(l -> {
				currentDto.setLatitude(l.location.latitude);
				currentDto.setLongitude(l.location.longitude);
				currentDtoList.add((CurrentLocationDTO) currentDto.clone());
			});
		}

		stopWatch.stop();

		assertThat(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime())).isTrue();

	}

}
