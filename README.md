# TOUR GUIDE

![image](https://user-images.githubusercontent.com/96872503/196967740-c29f5c39-0b95-4662-b5df-f882e847cbef.png)

Tour Guide aims at suggesting travel offers, putting forward attractions and shows with discounted fares.

## Release

version 0.0.1

## Technologies

- JAVA
- SPRING
- Gitlab CI/CD 

## Increment Scope

Current stage goal is to reduce response time and update supplied code.

## Contribute to the project

TourGuide is an open source project. Feel free to fork the source and contribute with your own features.

## Author

matsamb

## Licensing

This project was built under the MIT license.

